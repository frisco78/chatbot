class Bot {
  constructor(bot) {
    const { name, image } = bot;
    this.name = name;
    this.image = image;
    this.messages = localStorage.getItem(`message-${name}`) ? JSON.parse(localStorage.getItem(`message-${name}`)) : [];
    this.run();
  }

  sendMessage(event) {
    event.preventDefault();
    const textfield = document.getElementById(`textfield-${this.name}`);
    if (textfield.value === '') {
      return;
    }
    const data = {
      user: 1,
      message: textfield.value,
      date: new Date().toUTCString()
    };
    this.messages.push(data);
    localStorage.setItem(`message-${this.name}`, JSON.stringify(this.messages));
    this.run();
  }

  botMessage(text) {
    if (text === '') {
      return;
    }
    const data = {
      user: 0,
      message: text,
      date: new Date().toUTCString()
    };
    this.messages.push(data);
    localStorage.setItem(`message-${this.name}`, JSON.stringify(this.messages));
    this.run();
  }

  renderMessageCard(data) {
    return `
      <div class="card align-items-end" style="max-width: 60%;">
        <div class="card-header container-fluid">
          <div class="row">
            <img src="${data.user ? 'https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg' : this.image}" class="rounded-circle col-2" style="width: 80px; height: 55px;" alt="avatar"/>
            <div class="row col-10">
              <p class="card-text col-4">${data.user ? 'You' : this.name}</p>
              <p class="card-text col-8">${data.date}</p>
            </div>
          </div>
        </div>
        <div class="card-body">
          <p class="card-text">${data.message}</p>
        </div>
      </div>
    `;
  }

  renderMessageUser(data) {
    return `
    <div class="d-flex flex-row justify-content-end">
      ${this.renderMessageCard(data)}
    </div>
    `;
  }

  renderMessageBot(data) {
    return `
    <div class="d-flex flex-row justify-content-start">
      ${this.renderMessageCard(data)}
    </div>
    `;
  }

  renderMessages() {
    return `
    ${this.messages.toReversed().map((data) => {
    if (data.user === 0) {
      return this.renderMessageBot(data);
    }
    return this.renderMessageUser(data);
  })}`;
  }

  renderChatBox() {
    return `
      <p>${this.name.replace('-', ' ')}</p>
      <div class="container-fluid" style="margin-top: 2rem;">
        <form class="row">
        <div class="col-10">
          <input type="text" id="textfield-${this.name}" placeholder="Write your message" style="width: 90%"></textarea>
        </div>
        <div class="col-2">
          <button class="btn btn-primary" type="submit" id="submit-${this.name}">Send</button>
        </form>
      </div>
      <div id="messages">${this.renderMessages()}</div>
      </div>
    `;
  }

  run() {
    const div = document.getElementById(`list-${this.name}`);
    div.innerHTML = this.renderChatBox();

    const button = document.getElementById(`submit-${this.name}`);
    button.addEventListener('click', (event) => this.sendMessage(event));
  }
}

export default Bot;
