import Chat from './chat';
import Bot from './bot';
import Ryoma from './bots/Ryoma';
import WonderMomo from './bots/WonderMomo';
import Bocchi from './bots/Bocchi';

const bots = [
  {
    name: 'Wonder-Momo',
    image: 'https://s4.anilist.co/file/anilistcdn/character/large/87980.jpg',
    count: 0,
    class: 'WonderMomo'
  },
  {
    name: 'Ryouma-Nagare',
    image: 'https://s4.anilist.co/file/anilistcdn/character/large/b6107-jDp1qzjQAsxL.jpg',
    count: 0,
    class: 'Ryoma'
  },
  {
    name: 'Bocchi',
    image: 'https://s4.anilist.co/file/anilistcdn/character/large/b257562-97fPDk3TxIGR.png',
    count: 0,
    class: 'Bocchi'
  }
];

new Chat(bots);

bots.forEach((bot) => {
  switch (bot.class) {
    case 'Ryoma':
      new Ryoma(bot);
      break;
    case 'WonderMomo':
      new WonderMomo(bot);
      break;
    case 'Bocchi':
      new Bocchi(bot);
      break;
    default:
      new Bot(bot);
  }
});
