import Bot from '../bot';

class Bocchi extends Bot {
  sendMessage(event) {
    const textfield = document.getElementById(`textfield-${this.name}`);
    const message = textfield.value;
    super.sendMessage(event);

    switch (message.toLowerCase()) {
      case 'help':
        this.botMessage('Les Getter commandes disponibles sont: "Help" et "The Rock');
        break;
      case 'the rock':
        fetch('https://randomuser.me/api/')
          .then((response) => response.json())
          .then((response) => this.botMessage(`Voici une personne aléatoire: ${response.results[0].name.first} ${response.results[0].name.last}`));
        break;
      default:
        this.botMessage('Salut mon super pote');
        break;
    }
  }
}

export default Bocchi;
