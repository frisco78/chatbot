import Bot from '../bot';

class Ryoma extends Bot {
  sendMessage(event) {
    const textfield = document.getElementById(`textfield-${this.name}`);
    const message = textfield.value;
    super.sendMessage(event);

    switch (message.toLowerCase()) {
      case 'help':
        this.botMessage('Les Getter commandes disponibles sont: "Help" et "Getter');
        break;
      case 'getter':
        fetch('https://trouve-mot.fr/api/random')
          .then((response) => response.json())
          .then((words) => this.botMessage(`Le Getter mot est: ${words[0].name}`));
        break;
      default:
        this.botMessage('Salut mon super pote');
        break;
    }
  }
}

export default Ryoma;
