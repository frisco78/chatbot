import Bot from '../bot';

class WonderMomo extends Bot {
  sendMessage(event) {
    const textfield = document.getElementById(`textfield-${this.name}`);
    const message = textfield.value;
    super.sendMessage(event);

    switch (message.toLowerCase()) {
      case 'help':
        this.botMessage('Les Wonder commandes disponibles sont: "Help" et "WONDER MODE');
        break;
      case 'wonder mode':
        fetch('https://api.adviceslip.com/advice')
          .then((response) => response.json())
          .then((response) => this.botMessage(`Voici un nouveau conseil: ${response.slip.advice}`));
        break;
      default:
        this.botMessage('Salut mon super pote');
        break;
    }
  }
}

export default WonderMomo;
