import { Tab } from 'bootstrap';

const Chat = class {
  constructor(bots) {
    this.bots = bots;
    this.run();
  }

  renderBot(bot) {
    const { name, image, count } = bot;

    return `
    <li class="list-group-item d-flex justify-content-between align-items-center">
    <a class="list-group-item list-group-item-action" id="list-${name}-list" data-bs-toggle="list" href="#list-${name}" role="tab" aria-controls="list-${name}">
        <img width="80" src="${image}" alt="avatar-${name}"/>
        ${name.replace('-', ' ')}
        <span class="badge bg-primary rounded-pill">${count}</span>
      </a>
    </li>
    `;
  }

  renderBotChat(bot) {
    const { name } = bot;

    return `
      <div class="tab-pane fade show" id="list-${name}" role="tabpanel" aria-labelledby="list-${name}-list">
      </div>
    `;
  }

  renderBotList() {
    return `
      <div class="container-fluid" id="app">
        <div class="row">
          <div class="col-3">
            <ul class="list-group list-group-flush">
              ${this.bots.map((element) => (this.renderBot(element)))}
            </ul>
          </div>
          <div class="col-9 tab-content" id="nav-tabContent">
            ${this.bots.map((element) => (this.renderBotChat(element)))}
          </div>
        </div>
      </div>
    `;
  }

  render() {
    return `
      <nav class="navbar bg-body-tertiary">
        <div class="container">
          <p>Chatbot</p>
        </div>
      </nav>
      ${this.renderBotList()}
    `;
  }

  run() {
    document.body.innerHTML = this.render();

    const triggerTabList = document.querySelectorAll('#myTab a');
    triggerTabList.forEach((triggerEl) => {
      const tabTrigger = new Tab(triggerEl);

      triggerEl.addEventListener('click', (event) => {
        event.preventDefault();
        tabTrigger.show();
      });
    });
  }
};

export default Chat;
